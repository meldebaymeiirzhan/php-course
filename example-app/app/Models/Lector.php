<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Lector extends Model
{

  public $id;
  public $age;
  public $name;
  public $email;
  public $telephone;
  public $address;

  protected $table = 'lectors';
}
