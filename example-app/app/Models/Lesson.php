<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Lesson extends Model
{

  public $id;
  public $name;
  public $description;
  public $duration;
  public $date;
  public $time;
  public $teacher_id;

  protected $table = 'lessons';
}
