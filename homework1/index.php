<?php
// Проверьте, отрицательное оно или нет. Выведите об этом информацию в консоль
$given_number = 1;
if ($given_number > 0) {
  echo "Given number bigger zero!";
} else {
  echo "Given number smaller zero!";
}

//Выведите в консоль длину этой строки.
$given_string = "lorem ipsum";
echo strlen($given_string);

//Проверьте, есть ли в этой строке символ 'a'. Если есть, то выведите 'да', если нет, то 'нет'.
$given_string2 = "lorem ipsum";
$char = "a";
if (strpos($given_string2, $word) !== false) {
  echo "The string contains the char '{$char}'";
} else {
  echo "The string does not contain the char '{$char}'";
}

//Проверьте, что оно делится на 2, на 3, на 5, на 6, на 9 без остатка
$given_number1 = 90;
if ($given_number1 % 2 == 0 && $given_number1 % 3 == 0 && $given_number1 % 5 == 0 && $given_number1 % 6 == 0 && $given_number1 % 9 == 0) {
  echo "yes";
} else {
  echo "no";
}

$divisors = [2, 3, 4, 6, 9];
$allDivisible = true;

foreach ($divisors as $divisor) {
  if ($given_number % $divisor != 0) {
    $allDivisible = false;
    break;
  }
}

if ($allDivisible) {
  echo "All divisible\n";
} else {
  echo "Not divisible\n";
}

//Проверьте, что оно делится на 3, на 5, на 7, на 11 без остатка.
$given_number2 = 165;
$divisors = [3, 5, 7, 11];
//$allDivisible = true;

function checkDivisible($number, $array)
{
  $allDivisible = true;
  foreach ($array as $divisor) {
    if ($number % $divisor != 0) {
      $allDivisible = false;
      break;
    }
  }

  if ($allDivisible) {
    return "All divisible";
  } else {
    return "Not divisible";
  }
}

echo checkDivisible($given_number2, $divisors);


//Выведите в консоль первый символ строки.

$given_string3 = "checkDivisible";
echo $given_string3[0];


//Выведите в консоль последний символ строки.

$given_string3 = "checkDivisible";
echo substr($given_string3, -1);

//Дан треугольник. Выведите в консоль его площадь.
$a = 4;
$b = 5;

function square($a, $b, $type)
{
  if ($type == 'triangle') {
    return $a * $b / 2;
  }

  if ($type == 'rectangle') {
    return $a * $b;
  }
}

echo square($a, $b, "triangle");


//Дан прямоугольник. Выведите в консоль его площадь.
$a = 4;
$b = 15;


echo square($a, $b, "rectangle");


//Дано число. Выведите в консоль квадрат этого числа.
$a = 4;

function power($a)
{
  return $a * $a;
}

echo power($a);
