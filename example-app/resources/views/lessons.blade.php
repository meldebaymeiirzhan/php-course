<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <title>Laravel</title>

  <!-- Fonts -->
  <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">

  <!-- Styles -->
  <style>

  </style>

  <style>
    body {
      font-family: 'Nunito', sans-serif;
    }
  </style>
</head>

<body>
  <h1>
    Welcome to Lecture 2
  </h1>

  <p>
    Name Lesson : {{ $lesson['name']}}
  </p>

  <p>
    Name Teacher : {{ $teacher['name']}}
  </p>

  <table>
    <thead>
      <tr>
        <td>
          Name
        </td>
        <td>
          email
        </td>
      </tr>
    </thead>
    <tbody>
      @foreach($pupils as $pupil)
      <tr>
        <td>
          {{$pupil['name']}}
        </td>
        <td>
          {{$pupil['email']}}
        </td>
      </tr>
      @endforeach
    </tbody>
  </table>


  @if($isfinished)
  <p>
    Course is finished
  </p>
  @endif
</body>

</html>