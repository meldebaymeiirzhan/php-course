<?php


namespace App\Http\Controllers;

use App\Models\Pupil;


class SchoolController extends Controller
{

  public function lesson()
  {

    $pupils = Pupil::all();

    $teacher = \App\Models\Teacher::find(2);

    $lesson = \App\Models\Lesson::find(1);


    $isfinished = true;


    return view('lessons', compact('lesson', 'pupils', 'teacher', 'isfinished'));
  }
}
