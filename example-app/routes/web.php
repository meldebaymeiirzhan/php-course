<?php

use App\Http\Controllers\SchoolController;
use App\Models\Pupil;
use App\Models\Student;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
  return view('welcome');
});

Route::get('/lecture2', function () {

  $lecture = "Основы Ларавел";

  // $students = [
  //   'Damir',
  //   'Aktolkyn',
  //   'Elena'
  // ];

  // $students = [
  //   ['name' => 'Damir', 'email' => 'Damir@mail.ru'],
  //   ['name' => 'Aktolkyn', 'email' => 'Aktolkyn@mail.ru'],
  //   ['name' => 'Elena', 'email' => 'Elena@mail.ru']
  // ];

  $students = Student::all();

  dd($students);

  $isfinished = true;


  return view('lecture2', compact('lecture', 'students', 'isfinished'));
});


Route::get('/lessons', \App\Http\Controllers\SchoolController::class . '@lesson');
