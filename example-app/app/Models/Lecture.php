<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Lecture extends Model
{

  public $id;
  public $name;
  public $description;
  public $duration;
  public $date;
  public $time;
  public $lector_id;

  protected $table = 'lectures';
}
